package com.banking.authorizationservice.util;

import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.Map;

@Component("xmlWriterV1")
public class XmlWriter {
    public String writeXml(String type, Map<String, String> content) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            Element messageElement = doc.createElement("message");
            doc.appendChild(messageElement);
            messageElement.setAttribute("type", type);

            for (String key : content.keySet()) {
                Element element = doc.createElement(key);
                element.appendChild(doc.createTextNode(content.get(key)));
                messageElement.appendChild(element);
            }

            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(new DOMSource(doc), new StreamResult(sw));

            return sw.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
