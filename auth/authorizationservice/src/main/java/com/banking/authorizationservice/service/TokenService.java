package com.banking.authorizationservice.service;

import com.banking.authorizationservice.domain.User;
import com.banking.authorizationservice.util.exception.TokenExpiredException;
import com.banking.authorizationservice.util.exception.TokenInvalidException;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;

/**
 * Token Generator/Verifier
 *
 * @author Fawwaazrahman Arandhana W.
 * @version 1.0
 */

@Service
public class TokenService {

    @Value("${app.token.key}")
    String key;

    public String generate(User user) {
        try {
            Key aesKey = new SecretKeySpec(Arrays.copyOf(DigestUtils.sha256Hex(key.getBytes()).getBytes()
                    , 16), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String content = user.getUserId() + "|" + LocalDateTime.now().plusMinutes(60).format(formatter);
            System.out.println(content);

            return Base64.getEncoder().encodeToString(cipher.doFinal(content.getBytes("UTF-8")));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String verify(String token) throws TokenExpiredException, TokenInvalidException {
        try {
            Key aesKey = new SecretKeySpec(Arrays.copyOf(DigestUtils.sha256Hex(key.getBytes()).getBytes()
                    , 16), "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            String[] content = new String(cipher.doFinal(Base64.getDecoder().decode(token))).split("\\|");

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.parse(content[1], formatter);

            if (dateTime.isBefore(LocalDateTime.now())) {
                throw new TokenExpiredException("Expired Token");
            } else {
                return content[0];
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException ex) {
            ex.printStackTrace();
        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException ex) {
            throw new TokenInvalidException("Invalid Token");
        }

        return null;
    }
}