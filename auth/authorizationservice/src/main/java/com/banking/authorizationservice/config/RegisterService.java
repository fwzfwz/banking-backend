package com.banking.authorizationservice.config;

import com.banking.authorizationservice.util.Requester;
import com.banking.authorizationservice.util.XmlWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class RegisterService implements ApplicationListener<ApplicationStartedEvent> {

    @Autowired
    @Qualifier("xmlWriterV1")
    XmlWriter xmlWriter;

    @Autowired
    Requester requester;

    @Value("${discovery.default.address}")
    String address;

    @Value("${discovery.default.port}")
    String port;

    @Value("${discovery.default.route}")
    String route;

    @Value("${spring.rsocket.server.port}")
    int rsocketPort;

    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
        Map<String, String> requestContent = new HashMap<>();
        try {
            requestContent.put("address", InetAddress.getLocalHost().getHostAddress());
            requestContent.put("port", Integer.toString(rsocketPort));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        requester.requestReply(address, Integer.parseInt(port), route, xmlWriter.writeXml("REGISTER", requestContent));
    }
}
