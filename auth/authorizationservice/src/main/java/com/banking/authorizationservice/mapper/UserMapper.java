package com.banking.authorizationservice.mapper;

import com.banking.authorizationservice.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    /**
     * Search User by userId
     *
     * @param userId userId to search for
     * @return User result
     * @see com.banking.authorizationservice.domain.User
     */
    User selectByUserId(@Param("userId") String userId);

    /**
     * Search User by noRekening
     *
     * @param noRekening noRekening to search for
     * @return corresponding User with the same noRekening
     * @see com.banking.authorizationservice.domain.User
     */
    User selectByNoRekening(@Param("noRekening") String noRekening);

    /**
     * Search User by userId and Hashed pin
     *
     * @param userId userId to search for
     * @param pin    pin to search for
     * @return User that matches the parameter
     * @see com.banking.authorizationservice.domain.User
     */
    User selectByUserIdAndPin(@Param("userId") String userId, @Param("pin") String pin);

    /**
     * Insert new User
     *
     * @param user User data to add
     * @return affected rows
     * @see com.banking.authorizationservice.domain.User
     */
    int insertUser(User user);
}
