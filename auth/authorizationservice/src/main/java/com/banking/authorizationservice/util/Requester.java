package com.banking.authorizationservice.util;

import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicReference;

@Component
public class Requester {
    public String requestReply(String host, int port, String route, String content) {
        AtomicReference<String> result = new AtomicReference<>();
        Thread t = new Thread(() -> result.set(RSocketRequester.builder().tcp(host
                , port).route(route).data(content).retrieveMono(String.class).block()));
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result.get();
    }

    public void fireAndForget(String host, int port, String route, String content) {
        RSocketRequester rSocketRequester = RSocketRequester.builder().tcp(host, port);
        rSocketRequester.route(route).data(content).send();
    }
}
