package com.banking.authorizationservice.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Represents User Entity
 *
 * @author Fawwaazrahman Arandhana W.
 * @version 1.0
 */
@Data
@NoArgsConstructor
public class User {
    private String userId;
    private String noRekening;
    private LocalDateTime createdAt;
    private String pin;
}
