package com.banking.authorizationservice.service;

import com.banking.authorizationservice.util.exception.RekeningNotFoundException;
import com.banking.authorizationservice.util.exception.UserAlreadyExistException;
import com.banking.authorizationservice.util.exception.UserNotFoundException;
import com.banking.authorizationservice.domain.User;
import com.banking.authorizationservice.mapper.UserMapper;
import com.banking.authorizationservice.util.Requester;
import com.banking.authorizationservice.util.XmlParser;
import com.banking.authorizationservice.util.XmlWriter;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Element;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {

    @Autowired
    @Qualifier("xmlParserV1")
    XmlParser xmlParser;

    @Autowired
    @Qualifier("xmlWriterV1")
    XmlWriter xmlWriter;

    @Autowired
    UserMapper userMapper;

    @Autowired
    Requester requester;

    @Autowired
    TokenService tokenService;

    public String checkUserCredentials(User user) throws UserNotFoundException {
        String hashedPin = DigestUtils.sha256Hex(user.getPin());

        User result = userMapper.selectByUserIdAndPin(user.getUserId(), hashedPin);
        if (result != null) {
            return tokenService.generate(user);
        } else {
            throw new UserNotFoundException("User Not Found");
        }
    }

    public String insertUser(User user) throws RekeningNotFoundException, UserAlreadyExistException {
        Map<String, String> requestContent = new HashMap<>();
        requestContent.put("noRekening", user.getNoRekening());

        String result = requester.requestReply("localhost", 16000,
                "get-rekening-data", xmlWriter.writeXml("REGISTER", requestContent));

        Element root = xmlParser.parseXml(result).getDocumentElement();
        if (root.getAttribute("type").equals("SUCCESS")) {
            if (userMapper.selectByNoRekening(user.getNoRekening()) == null) {
                user.setCreatedAt(LocalDateTime.now());
                user.setPin(DigestUtils.sha256Hex(user.getPin()));
                userMapper.insertUser(user);
                return userMapper.selectByUserId(user.getUserId()).getUserId();
            } else {
                throw new UserAlreadyExistException("User With noRekening of " + user.getNoRekening()
                        + " Already exist");
            }
        } else {
            throw new RekeningNotFoundException("Rekening with noRekening of "
                    + user.getNoRekening() + " doesnt exist");
        }
    }

    public String getNoRekening(String userId) throws UserNotFoundException {
        User user = userMapper.selectByUserId(userId);
        if (user != null) {
            return user.getNoRekening();
        } else {
            throw new UserNotFoundException("User not found");
        }
    }
}
