package com.banking.authorizationservice.util.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Exception thrown when Token is expired
 *
 * @author Fawwaazrahman Arandhana W.
 * @version 1.0
 */

@AllArgsConstructor
@Getter
public class TokenExpiredException extends Exception {
    String message;
}
