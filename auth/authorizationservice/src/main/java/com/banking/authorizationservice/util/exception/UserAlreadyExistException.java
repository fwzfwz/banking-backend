package com.banking.authorizationservice.util.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Exception thrown when creating user with duplicate noRekening
 *
 * @author Fawwaazrahman Arandhana W.
 * @version 1.0
 */

@Getter
@AllArgsConstructor
public class UserAlreadyExistException extends Exception {
    String message;
}
