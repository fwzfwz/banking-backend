package com.banking.authorizationservice.util.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Exception thrown when user submit an invalid Token
 *
 * @author Fawwaazrahman Arandhana W.
 * @version 1.0
 */

@Getter
@AllArgsConstructor
public class TokenInvalidException extends Exception {
    String message;
}
