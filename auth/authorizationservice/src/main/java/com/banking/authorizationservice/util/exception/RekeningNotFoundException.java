package com.banking.authorizationservice.util.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RekeningNotFoundException extends Exception {
    String message;
}
