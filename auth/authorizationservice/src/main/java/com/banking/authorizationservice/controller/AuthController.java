package com.banking.authorizationservice.controller;

import com.banking.authorizationservice.service.TokenService;
import com.banking.authorizationservice.util.exception.*;
import com.banking.authorizationservice.domain.User;
import com.banking.authorizationservice.service.UserService;
import com.banking.authorizationservice.util.XmlParser;
import com.banking.authorizationservice.util.XmlWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.w3c.dom.Element;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Controller
public class AuthController {

    @Autowired
    UserService userService;

    @Autowired
    TokenService tokenService;

    @Autowired
    @Qualifier("xmlParserV1")
    XmlParser xmlParser;

    @Autowired
    @Qualifier("xmlWriterV1")
    XmlWriter xmlWriter;

    @MessageMapping("check-by-login")
    Mono<String> checkByLogin(final String payload) {
        Element root = xmlParser.parseXml(payload).getDocumentElement();

        User user = new User();
        user.setUserId(root.getElementsByTagName("userId").item(0).getTextContent());
        user.setPin(root.getElementsByTagName("pin").item(0).getTextContent());

        Map<String, String> responseContent = new HashMap<>();
        try {
            responseContent.put("token", userService.checkUserCredentials(user));
            return Mono.just(xmlWriter.writeXml("SUCCESS", responseContent));
        } catch (UserNotFoundException ex) {
            ex.printStackTrace();
            responseContent.put("message", ex.getMessage());
            return Mono.just(xmlWriter.writeXml("FAILED", responseContent));
        }
    }

    @MessageMapping("validate-token")
    Mono<String> validateToken(final String payload) {
        Element root = xmlParser.parseXml(payload).getDocumentElement();

        try {
            String userId = tokenService.verify(root.getElementsByTagName("token").item(0).getTextContent());

            Map<String, String> resultContent = new HashMap<>();
            resultContent.put("userId", userId);

            return Mono.just(xmlWriter.writeXml("SUCCESS", resultContent));
        } catch (TokenExpiredException ex) {
            Map<String, String> resultContent = new HashMap<>();
            resultContent.put("reason", ex.getMessage());

            return Mono.just(xmlWriter.writeXml("ERROR", resultContent));
        } catch (TokenInvalidException ex) {
            Map<String, String> resultContent = new HashMap<>();
            resultContent.put("reason", ex.getMessage());

            return Mono.just(xmlWriter.writeXml("ERROR", resultContent));
        }
    }

    @MessageMapping("create-new-user")
    Mono<String> createNewUser(final String payload) {
        Element root = xmlParser.parseXml(payload).getDocumentElement();

        User user = new User();
        user.setUserId(root.getElementsByTagName("userId").item(0).getTextContent());
        user.setPin(root.getElementsByTagName("pin").item(0).getTextContent());
        user.setNoRekening(root.getElementsByTagName("noRekening").item(0).getTextContent());

        Map<String, String> responseContent = new HashMap<>();
        try {
            String userId = userService.insertUser(user);
            responseContent.put("userId", userId);
            return Mono.just(xmlWriter.writeXml("SUCCESS", responseContent));
        } catch (RekeningNotFoundException ex) {
            responseContent.put("message", ex.getMessage());
            return Mono.just(xmlWriter.writeXml("FAILED", responseContent));
        } catch (UserAlreadyExistException ex) {
            responseContent.put("message", ex.getMessage());
            return Mono.just(xmlWriter.writeXml("FAILED", responseContent));
        }
    }

    @MessageMapping("get-no-rekening")
    Mono<String> getNoRekening(final String payload) {
        Element root = xmlParser.parseXml(payload).getDocumentElement();
        String userId = root.getElementsByTagName("userId").item(0).getTextContent();

        try {
            Map<String, String> resultContent = new HashMap<>();
            resultContent.put("noRekening", userService.getNoRekening(userId));

            return Mono.just(xmlWriter.writeXml("SUCCESS", resultContent));
        } catch (UserNotFoundException ex) {
            Map<String, String> resultContent = new HashMap<>();
            resultContent.put("reason", ex.getMessage());

            return Mono.just(xmlWriter.writeXml("FAILED", resultContent));
        }
    }
}
