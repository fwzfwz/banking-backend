package com.banking.authorization.discoveryservice.controller;

import com.banking.authorization.discoveryservice.domain.ServiceDomain;
import com.banking.authorization.discoveryservice.service.DiscoveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicReference;

@Controller
public class RequestController {

    @Autowired
    DiscoveryService discoveryService;

    @MessageMapping("{route}")
    public Mono<String> dispatchRequest(@DestinationVariable String route, @Payload final String payload) throws InterruptedException {
        ServiceDomain service = discoveryService.getService();
        AtomicReference<String> result = new AtomicReference<>();
        Thread t = new Thread(() -> result.set(RSocketRequester.builder().tcp(service.getAddress().getHostAddress()
                , service.getPort()).route(route).data(payload).retrieveMono(String.class).block()));
        t.start();
        t.join();

        System.out.println(result.get());
        return Mono.just(result.get());
    }
}