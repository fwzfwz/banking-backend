package com.banking.authorization.discoveryservice.controller;

import com.banking.authorization.discoveryservice.domain.ServiceDomain;
import com.banking.authorization.discoveryservice.service.DiscoveryService;
import com.banking.authorization.discoveryservice.util.XmlParser;
import com.banking.authorization.discoveryservice.util.XmlWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.w3c.dom.Element;
import reactor.core.publisher.Mono;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class DiscoveryController {

    @Autowired
    DiscoveryService discoveryService;

    @Autowired
    @Qualifier("xmlParserV1")
    XmlParser xmlParser;

    @Autowired
    @Qualifier("xmlWriterV1")
    XmlWriter xmlWriter;

    @MessageMapping("register-service")
    public Mono<Void> registerService(final String payload) throws UnknownHostException {
        System.out.println(payload);
        Element root = xmlParser.parseXml(payload).getDocumentElement();
        InetAddress address = InetAddress.getByName(root.getElementsByTagName("address").item(0).getTextContent());
        int port = Integer.parseInt(root.getElementsByTagName("port").item(0).getTextContent());
        discoveryService.addService(new ServiceDomain(address, port));
        return Mono.empty();
    }
}
