package com.banking.authorization.discoveryservice.service;

import com.banking.authorization.discoveryservice.domain.ServiceDomain;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.Queue;

@Service
public class DiscoveryService {
    private final Queue<ServiceDomain> serviceList = new LinkedList<>();

    public void addService(ServiceDomain address) {
        System.out.println("Old Size: " + serviceList.size());
        serviceList.offer(address);

        System.out.println(address.getAddress() + "," + address.getPort() + " Added");
        System.out.println("New Size: " + serviceList.size());
    }

    public ServiceDomain getService() {
        System.out.println("Old Size: " + serviceList.size());
        ServiceDomain address = serviceList.peek();

        try {
            System.out.println(address.getAddress() + "," + address.getPort() + " Received");
        } catch (NullPointerException ignored) {

        }
        System.out.println("New Size: " + serviceList.size());
        return address;
    }

    public void recycle() {
        ServiceDomain address = serviceList.poll();
        System.out.println("Old Size: " + serviceList.size());

        serviceList.offer(address);
        try {
            System.out.println(address.getAddress() + "," + address.getPort() + " Recycled");
        } catch (NullPointerException ignored) {

        }
        System.out.println("New Size: " + serviceList.size());
    }

    public void removeService() {
        System.out.println("Old Size: " + serviceList.size());
        ServiceDomain address = serviceList.poll();

        try {
            System.out.println(address.getAddress() + "," + address.getPort() + " Removed");
        } catch (NullPointerException ignored) {

        }
        System.out.println("New Size: " + serviceList.size());
    }
}
