package com.banking.authorization.discoveryservice.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.net.InetAddress;

@Getter
@AllArgsConstructor
public class ServiceDomain {
    InetAddress address;
    int port;
}
