package com.banking.authorization.discoveryservice.util;

import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.stereotype.Component;

@Component
public class Requester {

    public String requestReply(String host, int port, String route, String content) {
        RSocketRequester rSocketRequester = RSocketRequester.builder().tcp(host, port);
        return rSocketRequester.route(route).data(content).retrieveMono(String.class)
                .onErrorReturn("ERROR").block();
    }
}
