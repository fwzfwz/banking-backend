package com.banking.middle.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;

/**
 * Handle Incoming HTTP Request from vendor
 *
 * @author Fawwaazrahman Arandhana W.
 * @version 1.0
 */

@RestController
public class VendorController {

    @Value("${app.key}")
    String appKey;

    @Value("${app.vendorIdList}")
    String vendorIdList;

    @Value("${app.vendorHostList}")
    String vendorHostList;

    @Value("${app.vendorKey}")
    String vendorKey;

    @PostMapping("/rekening/")
    public ResponseEntity<?> getRekeningDetails(@RequestHeader("appKey") String appKey, @RequestBody final Map<String, String> payload) {
        if (appKey.equals(this.appKey)) {
            int index = Arrays.asList(vendorIdList.split(",")).indexOf(payload.get("vendorId"));

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add("appKey", vendorKey.split(",")[index]);
            HttpEntity<Map<String, String>> request = new HttpEntity<>(payload, headers);
            return restTemplate.exchange(vendorHostList.split(",")[index] + "/rekening/\n", HttpMethod.POST, request, Map.class);
        } else {
            return new ResponseEntity<>("Invalid Key", HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/transfer/")
    public ResponseEntity<?> createTransfer(@RequestHeader("appKey") String appKey, @RequestBody final Map<String, String> payload) {
        System.out.println(LocalDateTime.now().toString() + payload);
        if (appKey.equals(this.appKey)) {
            int index = Arrays.asList(vendorIdList.split(",")).indexOf(payload.get("receiverVendorId"));
            System.out.println("posttr" + vendorHostList.split(",")[index] + "/transfer/\n");
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add("appKey", vendorKey.split(",")[index]);
            HttpEntity<Map<String, String>> request = new HttpEntity<>(payload, headers);
            return restTemplate.exchange(vendorHostList.split(",")[index] + "/transfer/", HttpMethod.POST, request, Map.class);
        } else {
            return new ResponseEntity<>("Invalid Key", HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping("/transaksi/")
    public ResponseEntity<?> updateTransfer(@RequestHeader("appKey") String appKey, @RequestBody final Map<String, String> payload) {
        System.out.println(LocalDateTime.now().toString() + payload);
        if (appKey.equals(this.appKey)) {
            int index = Arrays.asList(vendorIdList.split(",")).indexOf(payload.get("receiverVendorId"));
            System.out.println("puttr" + vendorHostList.split(",")[index] + "/transaksi/\n");
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.add("appKey", vendorKey.split(",")[index]);
            HttpEntity<Map<String, String>> request = new HttpEntity<>(payload, headers);
            return restTemplate.exchange(vendorHostList.split(",")[index] + "/transaksi/", HttpMethod.PUT, request, Map.class);
        } else {
            return new ResponseEntity<>("Invalid Key", HttpStatus.FORBIDDEN);
        }
    }
}
