package com.example.client;

import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import reactor.core.publisher.Mono;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

@Component
public class Requester {
    private final RSocketRequester rsocketRequester;

    public Requester() {
        this.rsocketRequester = RSocketRequester.builder()
                .tcp("localhost", 16000);
    }

    public void request() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
        for (int i = 0; i < 1; i++) {
            int finalI = i;
//            executor.submit(() -> {
//                Mono<String> message = this.rsocketRequester.route("check-by-login")
//                        .data("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><payload><userId>631039798650</userId><pin>123456</pin></payload>")
//                        .retrieveMono(String.class);
//                System.out.println(finalI + "," + message.block());
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            });

//            executor.submit(() -> {
//                Mono<String> message = this.rsocketRequester.route("get-nasabahs-by-name")
//                        .data("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><payload><name>A</name></payload>")
//                        .retrieveMono(String.class);
//                System.out.println(finalI + "," + message.block());
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            });

//            executor.submit(() -> {
//                Mono<String> message = this.rsocketRequester.route("create-new-transaksi")
//                        .data("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><payload><receiverNoRekening>123</receiverNoRekening><receiverVendorId>002</receiverVendorId><senderNoRekening>123456789012</senderNoRekening><senderVendorId>001</senderVendorId><amount>10000</amount><status>FAILED</status></payload>")
//                        .retrieveMono(String.class);
//                System.out.println(finalI + "," + message.block());
//            });
            // try {
            //     Thread.sleep(1000);
            // } catch (InterruptedException e) {
            //     e.printStackTrace();
            // }

            executor.submit(() -> {
                Mono<String> message = this.rsocketRequester.route("get-rekening-data")
                        .data("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><payload><noRekening>123456789012</noRekening></payload>")
                        .retrieveMono(String.class);
                System.out.println(finalI + "," + message.block());
            });

            // executor.submit(() -> {
            //     Mono<String> message = this.rsocketRequester.route("receive-transfer")
            //             .data("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><payload><senderNoRekening>1111</senderNoRekening><senderVendorId>002</senderVendorId><receiverNoRekening>123456789012</receiverNoRekening><receiverVendorId>001</receiverVendorId><amount>15000</amount></payload>")
            //             .retrieveMono(String.class);
            //     System.out.println(finalI + "," + message.block());
            // });

            // executor.submit(() -> {
            //     Mono<String> message = this.rsocketRequester.route("check-by-login")
            //             .data("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><payload><userId>fwz123</userId><pin>123456</pin></payload>")
            //             .retrieveMono(String.class);
            //     System.out.println(finalI + "," + message.block());
            // });

            // executor.submit(() -> {
            //     Mono<String> message = this.rsocketRequester.route("validate-token")
            //             .data("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><payload><token>z6zbRDwZ4/o/IAM2EQ9Jx4XIUjYsihUnEjtBAFB4MSk=</token></payload>")
            //             .retrieveMono(String.class);
            //     System.out.println(finalI + "," + message.block());
            // });

            // executor.submit(() -> {
            //     Mono<String> message = this.rsocketRequester.route("create-new-user")
            //             .data("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><payload><userId>fwz123</userId><pin>123456</pin><noRekening>123456789012</noRekening></payload>")
            //             .retrieveMono(String.class);
            //     System.out.println(finalI + "," + message.block());
            // });
        }
    }
}
