package com.example.client;

public class Message {

    public String message;
    private long created;

    public Message(String message) {
        this.message = message;
    }

    public Message(String message, long created) {
        this.message = message;
        this.created = created;
    }

    public String getMessage() {
        return message;
    }

    public long getCreated() {
        return created;
    }
}